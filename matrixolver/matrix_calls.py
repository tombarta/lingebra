#coding: utf8
from .matrix import Matrix, ExtendedMatrix
import pdb
import sympy as sym
import numpy as np
import re


def inverse_call(M):
	if M.row_num==M.col_num:
		I = Matrix.identity(M.row_num)
		regularity = M.is_regular()
		if regularity['val']==False and regularity['why']=='rank':
			M.upper_triang_steps()	#chci oblbnout funkci gauss() v ExtendedMatrix, aby upravy skoncily u schodoviteho tvaru
			E = ExtendedMatrix(M,I)
			sol = E.gauss('upper_triang')
			sol[-1]['message'] = {'cz': 'Matice není regulární. Neexistuje k ní tedy inverzní'}
			error = 'irregular'
			return {'sol': sol, 'error': error}	#sol je jeden velky slovnik, aby v nem slo v templatu prehledne iterovat
		elif regularity['val']==True:
			E = ExtendedMatrix(M,I)
			sol = E.gauss()
			sol[-1]['message'] = {'cz': 'Na pravé straně je nyní inverzní matice'}
			error = 'irregular'
			return {'sol': sol}

def rank_call(M):
	sol = M.stairs()
	rank = M.rank()
	sol[-1]['message'] = {'cz': 'Hodnost matice je '+str(rank)}
	return {'sol': sol, 'rank': rank}

def solvesys_call(joined):
	M, vec = joined.col_split(joined.col_num-1)
	if M.rank()==joined.rank():	#frobeniova veta - zda existuje reseni
		M.gauss_steps()		#aby mela vypoctene piv_cols
		E = ExtendedMatrix(M,vec)
		sol = E.gauss()		#nedefinuji ale nic k message, jelikoz posledni zustane prazdna
		final_mx = sol[-1]['sol'].left.rows
		final_vec = [ sol[-1]['sol'].right.rows[i][0] for i in range(0,M.row_num) ]
		# nyni zjistit, zda ma matice dim(Ker)>0
		if M.rank()<M.col_num:
			var_list = ['x','y','z','u','v','w']
			param_list = ['t','s','r']
			piv_cols = M.piv_cols
			param_cols = list(set(range(0,M.col_num))-set(piv_cols))
			param_dict = { param_cols[i]:param_list[i] for i in range(0,len(param_cols)) }	#prirazeni parametru ke sloupcum
			print_arr = [[],[]]
			for i in range(0,len(piv_cols)):	#v tomto forcyklu se vytvori tabulka pred prevedenim parametru na druhou stranu
				print_arr[0].append([])
				for j in range(0,M.col_num):
					value = final_mx[i][j]
					if value!=0:
						if j==piv_cols[i]:
							print_arr[0][-1].append(var_list[i])
						else:
							sign = '+' if value>0 else '-'
							print_arr[0][-1].append(sign+str(value)+param_dict[j] if abs(value)!=1 else sign+param_dict[j])	#pokud bude koeficient +/-1, nezobrazi se
					else:
						print_arr[0][-1].append('')
				print_arr[0][-1].append('=')
				print_arr[0][-1].append(str(final_vec[i]))
			for i in range(0,len(piv_cols)):	#nyni se parametry prevedou na druhou stranu
				print_arr[1].append([])
				print_arr[1][-1].append(var_list[i])
				print_arr[1][-1].append('=')
				print_arr[1][-1].append(str(final_vec[i]))
				for j in range(0,len(param_cols)):
					value = final_mx[i][param_cols[j]]	#divam se na misto, kde ma byt koeficient j-teho parametru
					if value!=0:
						sign = '+' if value<0 else '-'	#znamenko je obracene, protoze parametry byly prehozeny na opacnou stranu
						print_arr[1][-1].append(sign+str(value)+param_dict[param_cols[j]] if abs(value)!=1 else sign+param_dict[param_cols[j]])
					else:
						print_arr[1][-1].append('')
			return {'sol': sol, 'A': M, 'param_tables': print_arr, 'params': {'num': len(param_cols), 'list': param_list[:len(param_cols)]}}
		else:
			return {'sol': sol, 'A': M}
	else:
		M.upper_triang_steps()
		E = ExtendedMatrix(M,vec)
		sol = E.gauss()
		error = 'frobenius'
		return {'sol': sol, 'A': M, 'error': error}

def eigenvals_call(M):
	lam = sym.var('xxx')
	newM = sym.Matrix(M.rows-lam*np.eye(M.row_num))
	polynomial = sym.det(newM)
	r = sym.roots(polynomial, lam)
	#r = sym.roots(polynomial, lam)
	LC = sym.LC(polynomial, lam)
	fact = sym.Mul(*[(lam-a.round(2))**r[a] for a in r])
#	with Capturing() as output1:
#		print_mathml(newM)

	output1 = newM.__str__()
	output1 = output1.replace('1.0*xxx', '<mi>&lambda;</mi>')
	output1 = output1.replace('([', r'<mfenced open="|" close="|" separators=""><mtable>')
	output1 = output1.replace('])', '</mtable></mfenced>')
	output1 = output1.replace('[', '<mtr><mtd>')
	output1 = output1.replace('], ', '</mtd></mtr>')
	output1 = output1.replace(']', '</mtd></mtr>')
	output1 = output1.replace(',', '</mtd><mtd>')
	output1 = output1.replace('+', '<mo>+</mo>')
	output1 = output1.replace('-', '<mo>-</mo>')
	output1 = output1.replace('Matrix', '')
	nums = re.findall('([0-9]*\.?([0-9]*)?)', output1)
	for num in set(nums):
		if num[0]!='': output1 = output1.replace(num[0], '<mn>'+num[0]+'</mn>')

	output2 = polynomial.__str__()
	exps = re.findall(r'(xxx(\*\*\d)?)', output2)
	for exp in exps:
		try:
			output2 = output2.replace(exp[0], '<msup><mi>&lambda;</mi>'+str(int(exp[0][-1]))+'</msup>')
		except:
			output2 = output2.replace(exp[0], '<mi>&lambda;</mi>')

	output2 = re.sub(r'([0-9]*\.?[0-9]+)', r'<mn>\1</mn>', output2)
#	for num in set(nums):
#		if num[0]!='': output2 = output2.replace(num[0], '<mn>'+num[0]+'</mn>')
	output2 = output2.replace('+', '<mo>+</mo>')
	output2 = output2.replace('-', '<mo>-</mo>')
	output2 = output2.replace('*', '')

	output3 = (LC*fact).__str__()
	output3 = re.sub(r'(\([a-z0-9-+\ \.\&]*\)(\*\*[0-9])?)', r'<msup>\1</msup>', output3)
	output3 = output3.replace('+', '<mo>+</mo>')
	output3 = output3.replace('-', '<mo>-</mo>')
	output3 = output3.replace('(', '<mrow><mo>(</mo>')
	output3 = output3.replace(')', '<mo>)</mo></mrow>')
	output3 = output3.replace('*', '')
	output3 = output3.replace('xxx', '<mi>&lambda;</mi>')
	output3 = re.sub(r'([0-9]*\.?[0-9]+)', r'<mn>\1</mn>', output3)
#	nums = re.findall('([0-9]*\.?([0-9]*)?)', output3)
#	for num in set(nums):
#		if num[0]!='': output3 = output3.replace(num[0], '<mn>'+num[0]+'</mn>')
	
#	output = '<mo>=</mo>'.join([output1, output2, output3]).replace('cn','mn').replace('ci','mi').replace('apply','mo').replace('<plus/>','mi').replace('<minus/>','mi')
	
	return {'mathml': '<mo>=</mo>'.join([output1, output2, output3])}
